package study.ktor

import io.ktor.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import study.ktor.endpoints.main
import study.ktor.endpoints.user

/**
 * @author jiahan.zhou
 * @date 2020/10/29
 */
fun main(args: Array<String>) {
    val env = applicationEngineEnvironment {
        // 使用多模块方式
        modules.addAll(modules())
        // 单Server, 可配置多个server
        connector {
            port = 7878
        }
    }
    // Web引擎Netty
    embeddedServer(Netty, env).start(wait = true)
}

fun modules(): Collection<Application.() -> Unit> {
    return listOf(Application::main, Application::user)
}