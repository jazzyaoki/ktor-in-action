package study.ktor.auth

import io.ktor.application.*
import io.ktor.auth.*

/**
 * 自定义Token认证
 * @author jiahan.zhou
 * @date 2020/11/16
 */
class TokenAuthenticationProvider internal constructor(configuration: Configuration) : AuthenticationProvider(configuration) {

    internal val authenticationFunction = configuration.authenticationFunction

    data class TokenCredentials(val accessToken: String, val refreshToken: String) : Credential

    class Configuration internal constructor(name: String?) : AuthenticationProvider.Configuration(name) {
        internal var authenticationFunction: AuthenticationFunction<TokenCredentials> = {
            throw NotImplementedError("Token auth validate function is not specified. Use token { validate { ... } } to fix.")
        }

        /**
         * Sets a validation function that will check given [TokenCredentials] instance and return [Principal],
         * or null if credential does not correspond to an authenticated principal
         */
        fun validate(body: suspend ApplicationCall.(TokenCredentials) -> Principal?) {
            authenticationFunction = body
        }
    }
}