package study.ktor.auth

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.auth.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.utils.io.charsets.*
import kotlin.text.Charsets

/**
 * 自定义Token验证规则
 * @author jiahan.zhou
 * @date 2020/11/16
 */
fun Authentication.Configuration.token(name: String? = null, configure: TokenAuthenticationProvider.Configuration.() -> Unit) {

    val provider = TokenAuthenticationProvider(TokenAuthenticationProvider.Configuration(name).apply(configure))
    val authenticate = provider.authenticationFunction

    val challengeKey: Any = "TokenAuth"

    provider.pipeline.intercept(AuthenticationPipeline.RequestAuthentication) { context ->
        val accessToken = call.request.header("X-Access-Token")
        val refreshToken = call.request.header("X-Refresh-Token")
        val credentials = if (isNotBlank(accessToken) && isNotBlank(refreshToken))
            TokenAuthenticationProvider.TokenCredentials(accessToken!!, refreshToken!!) else null

        val cause = if (null == credentials) {
            AuthenticationFailedCause.NoCredentials
        } else {
            val principal = authenticate(call, credentials)
            if (null == principal) {
                AuthenticationFailedCause.InvalidCredentials
            } else {
                context.principal(principal)
                null
            }
        }
        if (cause != null) {
            context.challenge(challengeKey, cause) {
                call.respond(UnauthorizedResponse(tokenAuthChallenge("Ktor Server", Charsets.UTF_8)))
                it.complete()
            }
        }
    }
    register(provider)
}

private fun isNotBlank(value: String?): Boolean {
    return null != value && value.isNotBlank()
}

/**
 * Generates an BearerToken challenge as a [HttpAuthHeader].
 */
fun tokenAuthChallenge(realm: String, charset: Charset?): HttpAuthHeader.Parameterized = HttpAuthHeader.Parameterized(
        "Authentication", LinkedHashMap<String, String>().apply {
    put(HttpAuthHeader.Parameters.Realm, realm)
    if (charset != null) {
        put(HttpAuthHeader.Parameters.Charset, charset.name)
    }
}
)

data class LoginUser(val id: Long, val name: String) : Principal