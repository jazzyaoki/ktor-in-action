package study.ktor.beans

/**
 * @author jiahan.zhou
 * @date 2020/11/15
 */
data class User(var id: Long?, var name: String)