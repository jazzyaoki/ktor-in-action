package study.ktor.endpoints

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.slf4j.event.Level
import study.ktor.auth.LoginUser
import study.ktor.auth.token
import java.util.*

/**
 * web主配置, 定义基础的功能以及配置
 * @author jiahan.zhou
 * @date 2020/11/15
 */
fun Application.main() {
    // 安装序列化插件
    install(ContentNegotiation) {
        // 使用Gson序列化
        gson {
            setPrettyPrinting()
        }
    }
    // 安装请求ID记录
    install(CallId) {
        // 使用UUID生成
        retrieve {
            UUID.randomUUID().toString().replace("-", "")
        }
        // 添加到response Header中(x-request-id)
        replyToHeader(HttpHeaders.XRequestId)
    }
    // 安装请求日志
    install(CallLogging) {
        level = Level.INFO
        // 添加到logback MDC上下文中
        callIdMdc("mdc-call-id")
    }
    // 添加默认响应头(Date..,etc)
    install(DefaultHeaders)
    // 安装http状态码处理器
    install(StatusPages) {
        // 异常处理
        exception<Throwable> {
            call.respond(HttpStatusCode.InternalServerError, "Oops, Something wrong~")
        }
    }
    // 安装认证
    install(Authentication) {
        // 使用 token
        token {
            // 跳过健康检测接口 /hb
            validate {
                log.info("Credentials: {}", it)
                // TODO 使用redis
                return@validate when (it.accessToken) {
                    it.refreshToken -> LoginUser(1, "A")
                    "ABC" -> LoginUser(2, "B")
                    else -> null
                }
            }
        }
    }
    routing {
        // 健康检测
        get("/hb") {
            call.respond(HttpStatusCode.OK)
        }
    }
}