package study.ktor.endpoints

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import org.slf4j.LoggerFactory
import study.ktor.auth.LoginUser
import study.ktor.beans.User

/**
 * @author jiahan.zhou
 * @date 2020/11/15
 */
fun Application.user() {
    val log = LoggerFactory.getLogger(this.javaClass)
    routing {
        // 启用认证
        authenticate {
            route("user") {
                get("/profile") {
                    // 从请求中获取已认证用户
                    val principal = call.principal<LoginUser>()!!
                    call.respond(User(principal.id, principal.name))
                }
            }
        }
    }
}

